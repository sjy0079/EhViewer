package com.hippo.ehviewer.ui.viewpagergallery

import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.transition.*
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.hippo.ehviewer.R
import com.hippo.ehviewer.Settings
import com.hippo.ehviewer.ui.EhActivity
import com.hippo.util.SystemUiHelper
import rikka.core.res.resolveColor
import rikka.material.app.DayNightDelegate

abstract class VPGBasicActivity: EhActivity() {
    var systemUiHelper: SystemUiHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Settings.getReadingFullscreen()) {
            with(window) {
                setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
                )
                setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                )
            }
        }
        super.onCreate(savedInstanceState)
        readSettings()
        // set transition
        with(window) {
            requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
            enterTransition = Fade()
            exitTransition = Fade()
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.trans_move)
            sharedElementExitTransition = TransitionInflater.from(context).inflateTransition(R.transition.trans_move)
        }
    }

    override fun attachBaseContext(newBase: Context) {
        when (Settings.getReadTheme()) {
            1 -> dayNightDelegate.setLocalNightMode(DayNightDelegate.MODE_NIGHT_YES, false)
            2 -> dayNightDelegate.setLocalNightMode(DayNightDelegate.MODE_NIGHT_NO, false)
        }
        super.attachBaseContext(newBase)
    }

    override fun respectDefaultNightMode(): Boolean {
        return Settings.getReadTheme() == 0
    }

    private fun readSettings() {
        // Orientation
        requestedOrientation = when (Settings.getScreenRotation()) {
            0 -> ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            1 -> ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
            2 -> ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
            3 -> ActivityInfo.SCREEN_ORIENTATION_SENSOR
            else -> ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        }

        // System UI helper
        if (Settings.getReadingFullscreen()) {
            systemUiHelper = SystemUiHelper(this)
            systemUiHelper?.hide()
        } else {
            val window = window
            val decorView = window.decorView
            var flags = decorView.systemUiVisibility
            flags = if (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_YES <= 0) {
                flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
            }
            decorView.systemUiVisibility = flags
            window.statusBarColor = theme.resolveColor(android.R.attr.colorBackground)
        }
    }
}