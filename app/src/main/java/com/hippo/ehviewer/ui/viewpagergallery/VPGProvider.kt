package com.hippo.ehviewer.ui.viewpagergallery

import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.os.Message
import com.hippo.ehviewer.EhApplication
import com.hippo.ehviewer.R
import com.hippo.ehviewer.client.data.GalleryInfo
import com.hippo.ehviewer.gallery.EhGalleryProvider
import com.hippo.image.Image

class VPGProvider(
    private val handler: Handler,
    context: Context,
    private val galleryInfo: GalleryInfo
) :
    EhGalleryProvider(context, galleryInfo) {
    companion object {
        const val MSG_DATA_INDEX_KEY = "msg_data_index_key"

        const val MSG_ON_GET_PAGES = 0
        const val MSG_DATA_PAGES_KEY = "msg_data_pages_key"

        const val MSG_ON_PAGE_DOWNLOAD = 1
        const val MSG_DATA_PAGE_DOWNLOAD_PERCENT_KEY = "msg_data_page_download_percent_key"

        const val MSG_ON_GET_IMAGE_SUCCESS = 2
        const val MSG_ON_PAGE_FAILURE = 3
    }

    override fun onGetImageSuccess(index: Int, image: Image) {
        val bitmap = Bitmap.createBitmap(image.width, image.height, Bitmap.Config.ARGB_8888)
        image.render(0, 0, bitmap, 0, 0, image.width, image.height, false, 0)
        VPGCache.removeError(galleryInfo, index)
        VPGCache.putImageCache(galleryInfo, index, bitmap)

        val msg = Message.obtain().apply {
            what = MSG_ON_GET_IMAGE_SUCCESS
            data.putInt(
                MSG_DATA_INDEX_KEY,
                index
            )
        }
        handler.sendMessage(msg)
    }

    override fun onPageDownload(
        index: Int,
        contentLength: Long,
        receivedSize: Long,
        bytesRead: Int
    ) {
        val msg = Message.obtain().apply {
            what = MSG_ON_PAGE_DOWNLOAD
            data.putInt(
                MSG_DATA_INDEX_KEY,
                index
            )
            data.putInt(
                MSG_DATA_PAGE_DOWNLOAD_PERCENT_KEY,
                ((receivedSize.toFloat() / contentLength) * 10).toInt()
            )
        }
        handler.sendMessage(msg)
    }

    override fun onGetPages(pages: Int) {
        val msg = Message.obtain().apply {
            what = MSG_ON_GET_PAGES
            data.putInt(MSG_DATA_PAGES_KEY, pages)
        }
        handler.sendMessage(msg)
    }

    override fun onPageSuccess(index: Int, finished: Int, downloaded: Int, total: Int) {
        request(index)
    }

    override fun onPageFailure(
        index: Int,
        error: String?,
        finished: Int,
        downloaded: Int,
        total: Int
    ) {
        VPGCache.putError(
            galleryInfo, index,
            error ?: EhApplication.getInstance().resources.getString(R.string.error_unknown)
        )
        val msg = Message.obtain().apply {
            what = MSG_ON_PAGE_FAILURE
            data.putInt(MSG_DATA_INDEX_KEY, index)
        }
        handler.sendMessage(msg)
    }

    override fun onGetImageFailure(index: Int, error: String?) {
        VPGCache.putError(
            galleryInfo, index,
            error ?: EhApplication.getInstance().resources.getString(R.string.error_unknown)
        )
        val msg = Message.obtain().apply {
            what = MSG_ON_PAGE_FAILURE
            data.putInt(MSG_DATA_INDEX_KEY, index)
        }
        handler.sendMessage(msg)
    }

    override fun stop() {
        super.stop()
        VPGCache.clearImageCache(galleryInfo)
        VPGCache.clearError(galleryInfo)
    }
}