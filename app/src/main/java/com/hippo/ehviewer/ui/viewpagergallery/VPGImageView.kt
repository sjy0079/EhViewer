package com.hippo.ehviewer.ui.viewpagergallery

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.Scroller
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.GestureDetectorCompat
import com.hippo.ehviewer.R
import kotlin.math.round


class VPGImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {
    companion object {
        const val TRANS_NAME = "vpg_image_view_trans"

        var transitionImage: Bitmap? = null
            get() {
                val temp = field
                field = null
                return temp
            }
    }

    private var isAnimating = false
    private var targetScaleStep1: Float = -1F
    private val targetScaleStep2: Float
        get() {
            return targetScaleStep1 * 2
        }

    private var scrollXMax = 0
    private var scrollYMax = 0
    private val scroller = Scroller(context)
    private var isFlinging = false

    var isDetailMode = false

    private var gestureDetector =
        GestureDetectorCompat(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onDown(e: MotionEvent?): Boolean {
                scroller.forceFinished(true)
                return super.onDown(e)
            }

            override fun onDoubleTap(e: MotionEvent?): Boolean {
                if (!isDetailMode || targetScaleStep1 <= 1F || targetScaleStep2 <= 1F) {
                    return false
                }
                scroller.forceFinished(true)
                if (scaleX >= 1F && scaleX < targetScaleStep1) {
                    playScaleAnimate(targetScaleStep1)
                } else if (scaleX >= targetScaleStep1 && scaleX < targetScaleStep2) {
                    playScaleAnimate(targetScaleStep2)
                } else {
                    playScaleAnimate(1F)
                }
                return super.onDoubleTap(e)
            }

            override fun onScroll(
                e1: MotionEvent?,
                e2: MotionEvent?,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                if (!isDetailMode || isFlinging || isAnimating) {
                    return false
                }
                if (isOutBorderX(distanceX)) {
                    scrollX += distanceX.toInt()
                }
                if (isOutBorderY(distanceY)) {
                    scrollY += distanceY.toInt()
                }
                return super.onScroll(e1, e2, distanceX, distanceY)
            }

            override fun onFling(
                e1: MotionEvent?,
                e2: MotionEvent?,
                velocityX: Float,
                velocityY: Float
            ): Boolean {
                if (!isDetailMode || isFlinging || isAnimating) {
                    return false
                }
                calcBorder()
                scroller.fling(
                    scrollX, scrollY,
                    -velocityX.toInt(), -velocityY.toInt(),
                    -scrollXMax, scrollXMax, -scrollYMax, scrollYMax
                )
                postInvalidateOnAnimation()
                return super.onFling(e1, e2, velocityX, velocityY)
            }

            private fun isOutBorderX(distanceX: Float): Boolean {
                if (distanceX == 0F) {
                    return false
                }
                calcBorder()
                val n = if (distanceX < 0) -1 else 1
                val realScrollXMaX = scrollXMax
                val isOutXBorder = realScrollXMaX > (scrollX + distanceX) * n
                if (!isOutXBorder) {
                    scrollX = (realScrollXMaX * n)
                }
                return isOutXBorder
            }

            private fun isOutBorderY(distanceY: Float): Boolean {
                if (distanceY == 0F) {
                    return false
                }
                calcBorder()
                val n = if (distanceY < 0) -1 else 1
                val realScrollYMax = scrollYMax
                val isOutYBorder = realScrollYMax > (scrollY + distanceY) * n
                if (!isOutYBorder) {
                    scrollY = (realScrollYMax * n)
                }
                return isOutYBorder
            }
        })

    init {
        isClickable = true
        isFocusable = true
        transitionName = TRANS_NAME

        setOnLongClickListener {
            if (isDetailMode) {
                return@setOnLongClickListener false
            }
            val imageTag = super.getTag(R.id.vpg_image_tag)
            if (imageTag !is Bitmap) {
                return@setOnLongClickListener false
            }
            transitionImage = imageTag
            val intent = Intent(context, VPGDetailActivity::class.java)
            val options = ActivityOptions
                .makeSceneTransitionAnimation(context as Activity, this, TRANS_NAME)
            // start the detail activity
            context.startActivity(intent, options.toBundle())
            true
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        transitionImage = null
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        gestureDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        super.setTag(R.id.vpg_image_tag, bm)
        countScale()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        countScale()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        countScale()
    }

    override fun computeScroll() {
        super.computeScroll()
        if (scroller.computeScrollOffset()) {
            isFlinging = true
            scrollTo(scroller.currX, scroller.currY)
            invalidate()
        } else {
            isFlinging = false
        }
    }

    private fun countScale() {
        val imageTag = super.getTag(R.id.vpg_image_tag)
        if (imageTag !is Bitmap || height <= 0 || width <= 0) {
            return
        }
        val viewRatioW = width.toFloat() / height
        val imageRatioW = imageTag.width.toFloat() / imageTag.height
        targetScaleStep1 =
            if (imageRatioW > viewRatioW) {
                imageRatioW / viewRatioW
            } else {
                viewRatioW / imageRatioW
            }
    }

    private fun calcBorder() {
        val imageTag = getTag(R.id.vpg_image_tag)
        if (imageTag !is Bitmap) {
            return
        }
        val viewRatioW = width.toFloat() / height
        val imageRatioW = imageTag.width.toFloat() / imageTag.height
        val imageRenderH = if (viewRatioW > imageRatioW) {
            height.toFloat()
        } else {
            width / imageRatioW
        } * scaleY
        val imageRenderW = if (viewRatioW > imageRatioW) {
            height * imageRatioW
        } else {
            width.toFloat()
        } * scaleX

        scrollXMax = if (imageRenderW < width) {
            0
        } else {
            round((imageRenderW - width) / 2 / scaleX)
        }.toInt()
        scrollYMax = if (imageRenderH < height) {
            0
        } else {
            round((imageRenderH - height) / 2 / scaleY)
        }.toInt()
    }

    private fun playScaleAnimate(target: Float) {
        if (isAnimating) {
            return
        }
        val animators = arrayListOf<Animator>()
        createAnimator(
            scaleX, target,
            onUpdate = { scaleX = it; scaleY = it },
            onStart = { isAnimating = true },
            onEnd = { scaleX = target; scaleY = target; isAnimating = false }
        )?.let {
            animators.add(it)
        }
        if (target == 1F) {
            createAnimator(
                scrollX, 0,
                onUpdate = { scrollX = it },
                onEnd = { scrollX = 0 }
            )?.let {
                animators.add(it)
            }
            createAnimator(
                scrollY, 0,
                onUpdate = { scrollY = it },
                onEnd = { scrollY = 0 }
            )?.let {
                animators.add(it)
            }
        }
        animators.takeIf { it.size > 0 }.let {
            AnimatorSet().apply { playTogether(it) }.start()
        }
    }

    @SuppressLint("Recycle")
    @Suppress("UNCHECKED_CAST")
    private fun <T> createAnimator(
        from: T,
        to: T,
        onUpdate: (T) -> Unit,
        onStart: ((Animator) -> Unit)? = null,
        onEnd: ((Animator) -> Unit)? = null
    ): ValueAnimator? {
        val animator =
            when (from!!::class) {
                Int::class -> {
                    ValueAnimator.ofInt(from as Int, to as Int)
                }
                Float::class -> {
                    ValueAnimator.ofFloat(from as Float, to as Float)
                }
                else -> {
                    return null
                }
            }
        return animator.apply {
            duration = 200
            addUpdateListener {
                onUpdate.invoke(it.animatedValue as T)
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator) {
                    onStart?.invoke(animation)
                }

                override fun onAnimationEnd(animation: Animator) {
                    onEnd?.invoke(animation)
                }

                override fun onAnimationCancel(animation: Animator) {
                    onEnd?.invoke(animation)
                }
            })
        }
    }
}