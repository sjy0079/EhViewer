package com.hippo.ehviewer.ui.viewpagergallery

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.material.progressindicator.CircularProgressIndicator
import com.hippo.ehviewer.R
import com.hippo.ehviewer.client.data.GalleryInfo

class VPGPageView
@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    companion object {
        fun setupImageViewByBitmap(
            imageView: VPGImageView,
            bitmap: Bitmap,
            isStart: Boolean,
            isRTL: Boolean,
            isLastSinglePage: Boolean
        ) {
            if (imageView.measuredWidth == 0) {
                imageView.post {
                    setupImageViewByBitmap(imageView, bitmap, isStart, isRTL, isLastSinglePage)
                }
                return
            }
            if (bitmap.width.toFloat() / bitmap.height <
                imageView.measuredWidth.toFloat() / imageView.measuredHeight &&
                !isLastSinglePage
            ) {
                val isRealStart = if (isRTL) {
                    !isStart
                } else {
                    isStart
                }
                if (isRealStart) {
                    imageView.scaleType = ImageView.ScaleType.FIT_END
                } else {
                    imageView.scaleType = ImageView.ScaleType.FIT_START
                }
            } else {
                imageView.scaleType = ImageView.ScaleType.FIT_CENTER
            }
            imageView.setImageBitmap(bitmap)
        }
    }

    val leftHolder: Holder
    val rightHolder: Holder

    init {
        inflate(context, R.layout.vpg_page_layout, this)
        leftHolder = Holder(
            findViewById(R.id.left_page),
            findViewById(R.id.left_image),
            findViewById(R.id.left_info),
            findViewById(R.id.left_progress),
            findViewById(R.id.left_text),
            findViewById(R.id.left_error)
        )
        rightHolder = Holder(
            findViewById(R.id.right_page),
            findViewById(R.id.right_image),
            findViewById(R.id.right_info),
            findViewById(R.id.right_progress),
            findViewById(R.id.right_text),
            findViewById(R.id.right_error)
        )
    }

    class Holder(
        val thisView: View,
        val image: VPGImageView,
        val infoContainer: View,
        val progress: CircularProgressIndicator,
        val infoText: TextView,
        val errorText: TextView
    )
}