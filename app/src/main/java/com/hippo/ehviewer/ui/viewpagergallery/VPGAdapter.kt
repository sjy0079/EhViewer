package com.hippo.ehviewer.ui.viewpagergallery

import android.graphics.Bitmap
import android.util.LruCache
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hippo.ehviewer.client.data.GalleryInfo
import com.hippo.ehviewer.ui.viewpagergallery.VPGPageView.Companion.setupImageViewByBitmap
import com.hippo.glgallery.GalleryView
import java.lang.ref.WeakReference

class VPGAdapter(
    private val galleryInfo: GalleryInfo,
    private val provider: VPGProvider,
    private val galleryPages: Int,
    @GalleryView.LayoutMode private val readDirection: Int
) :
    RecyclerView.Adapter<VPGAdapter.ViewHolder>() {
    private val holderCache = LruCache<Int, WeakReference<VPGPageView.Holder>>(50)

    inner class ViewHolder(val pageView: VPGPageView) :
        RecyclerView.ViewHolder(pageView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(VPGPageView(parent.context).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        })
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val leftPos = position * 2
        val rightPos = position * 2 + 1

        holder.pageView.leftHolder.setupPageHolder(leftPos)
        holder.pageView.rightHolder.setupPageHolder(rightPos)
    }

    override fun getItemCount(): Int = (galleryPages + 1) / 2

    fun bindBitmapFromCache(position: Int) {
        holderCache[position]?.get()?.apply {
            if (thisView.tag == position) {
                setHolderImageView(
                    image,
                    position,
                    VPGCache.getImageCache(galleryInfo, position)
                )
                infoContainer.visibility = View.GONE
            }
        }
    }

    fun bindProgressFromCache(position: Int, progressPercent: Int) {
        holderCache[position]?.get()?.apply {
            if (thisView.tag == position) {
                image.setImageBitmap(null)
                infoContainer.visibility = View.VISIBLE
                progress.apply {
                    isIndeterminate = false
                    visibility = View.VISIBLE
                    progress = progressPercent
                }
            }
        }
    }

    fun bindErrorFromCache(position: Int) {
        holderCache[position]?.get()?.apply {
            if (thisView.tag == position) {
                setErrorView(position)
            }
        }
    }

    fun destroy() {
        holderCache.evictAll()
    }

    private fun VPGPageView.Holder.setupPageHolder(position: Int) {
        if (position >= galleryPages && position % 2 == 1) {
            thisView.visibility = View.GONE
            return
        }
        holderCache.put(position, WeakReference(this))
        thisView.tag = position
        thisView.visibility = View.VISIBLE
        infoText.text = (position + 1).toString()

        if (setErrorView(position)) {
            return
        }

        if (VPGCache.getImageCache(galleryInfo, position) != null) {
            setHolderImageView(
                image,
                position,
                VPGCache.getImageCache(galleryInfo, position)
            )
            infoContainer.visibility = View.GONE
        } else {
            image.setImageBitmap(null)
            infoContainer.visibility = View.VISIBLE
            progress.apply {
                isIndeterminate = true
                visibility = View.VISIBLE
            }
            provider.request(position)
        }
    }

    /**
     * @return isError
     */
    private fun VPGPageView.Holder.setErrorView(position: Int): Boolean {
        if (VPGCache.getError(galleryInfo, position).isBlank()) {
            return false
        }
        image.setImageBitmap(null)
        infoContainer.visibility = View.VISIBLE
        errorText.apply {
            text = VPGCache.getError(galleryInfo, position)
            visibility = View.VISIBLE
            setOnClickListener {
                this.visibility = View.GONE
                progress.apply {
                    isIndeterminate = true
                    visibility = View.VISIBLE
                }
                provider.forceRequest(position)
            }
        }
        progress.visibility = View.GONE
        return true
    }

    private fun setHolderImageView(
        imageView: VPGImageView,
        position: Int,
        bitmap: Bitmap?
    ) {
        bitmap ?: return
        setupImageViewByBitmap(
            imageView, bitmap,
            isStart = position % 2 == 0,
            isRTL = readDirection == GalleryView.LAYOUT_RIGHT_TO_LEFT,
            isLastSinglePage = position + 1 == galleryPages && position % 2 == 0
        )
    }
}