package com.hippo.ehviewer.ui.viewpagergallery

import android.graphics.Bitmap
import android.util.LruCache
import com.hippo.ehviewer.client.data.GalleryInfo

object VPGCache {
    private val bitmapCache = HashMap<Long, LruCache<Int, Bitmap>>()
    private val errorCache = HashMap<Long, HashMap<Int, String>>()

    fun putImageCache(galleryInfo: GalleryInfo, index: Int, bitmap: Bitmap) {
        val gid = galleryInfo.gid
        if (bitmapCache[gid] == null) {
            bitmapCache[gid] = LruCache(50)
        }
        bitmapCache[gid]!!.put(index, bitmap)
    }

    fun getImageCache(galleryInfo: GalleryInfo, index: Int): Bitmap? {
        val cache = bitmapCache[galleryInfo.gid] ?: return null
        return cache[index]
    }

    fun clearImageCache(galleryInfo: GalleryInfo) {
        bitmapCache[galleryInfo.gid]?.evictAll()
    }

    fun putError(galleryInfo: GalleryInfo, index: Int, error: String) {
        val gid = galleryInfo.gid
        if (errorCache[gid] == null) {
            errorCache[gid] = HashMap()
        }
        errorCache[gid]!![index] = error
    }

    fun getError(galleryInfo: GalleryInfo, index: Int): String {
        val cache = errorCache[galleryInfo.gid] ?: return String()
        return cache[index] ?: String()
    }

    fun removeError(galleryInfo: GalleryInfo, index: Int) {
        val cache = errorCache[galleryInfo.gid] ?: return
        cache.remove(index)
    }

    fun clearError(galleryInfo: GalleryInfo) {
        errorCache[galleryInfo.gid]?.clear()
    }
}