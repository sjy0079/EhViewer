package com.hippo.ehviewer.ui.viewpagergallery

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.FrameLayout
import kotlin.math.abs

class VPGContainer @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    private var dx: Float = -2F
    private var dy: Float = -2F

    var onMidTouchListener: (() -> Unit)? = null
    var onBottomTouchListener: (() -> Unit)? = null
    var onDragListener: (() -> Unit)? = null

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        when (ev.action) {
            MotionEvent.ACTION_DOWN -> {
                dx = ev.x
                dy = ev.y
            }
            MotionEvent.ACTION_MOVE -> {
                if (abs(ev.x - dx) >= 2F || abs(ev.y - dy) >= 2F) {
                    onDragListener?.invoke()
                }
            }
            MotionEvent.ACTION_UP -> {
                val isClickNotDrag = abs(ev.x - dx) < 2F && abs(ev.y - dy) < 2F
                if (isClickNotDrag) {
                    val percent15 = measuredHeight * 0.15F
                    if (ev.y > percent15 && ev.y + percent15 < measuredHeight) {
                        onMidTouchListener?.invoke()
                    } else if (ev.y + percent15 >= measuredHeight) {
                        onBottomTouchListener?.invoke()
                    }
                }
                dx = -2F
                dy = -2F
            }
        }
        return super.dispatchTouchEvent(ev)
    }
}