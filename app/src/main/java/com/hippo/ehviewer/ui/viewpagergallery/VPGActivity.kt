package com.hippo.ehviewer.ui.viewpagergallery

import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.*
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.hippo.ehviewer.EhApplication
import com.hippo.ehviewer.Settings
import com.hippo.ehviewer.client.data.GalleryInfo
import com.hippo.ehviewer.ui.GalleryActivity
import com.hippo.ehviewer.ui.viewpagergallery.VPGProvider.Companion.MSG_DATA_INDEX_KEY
import com.hippo.ehviewer.ui.viewpagergallery.VPGProvider.Companion.MSG_DATA_PAGES_KEY
import com.hippo.ehviewer.ui.viewpagergallery.VPGProvider.Companion.MSG_DATA_PAGE_DOWNLOAD_PERCENT_KEY
import com.hippo.ehviewer.ui.viewpagergallery.VPGProvider.Companion.MSG_ON_GET_IMAGE_SUCCESS
import com.hippo.ehviewer.ui.viewpagergallery.VPGProvider.Companion.MSG_ON_GET_PAGES
import com.hippo.ehviewer.ui.viewpagergallery.VPGProvider.Companion.MSG_ON_PAGE_DOWNLOAD
import com.hippo.ehviewer.ui.viewpagergallery.VPGProvider.Companion.MSG_ON_PAGE_FAILURE
import com.hippo.glgallery.GalleryView
import com.hippo.yorozuya.LayoutUtils
import com.hippo.yorozuya.SimpleHandler

class VPGActivity : VPGBasicActivity() {
    private var action: String = String()
    private var filename: String = String()
    private var uri: Uri = Uri.EMPTY
    private var galleryInfo: GalleryInfo? = null
    private var page: Int = -1
    private var currentIndex: Int = 0
    private var showSystemUi = false
    private val readDirection = Settings.getReadingDirection()

    private lateinit var container: FrameLayout
    private lateinit var galleryView: ViewPager2
    private lateinit var headerView: VPGHeaderView
    private lateinit var seekbar: VPGSeekbar
    private var galleryAdapter: VPGAdapter? = null
    private lateinit var provider: VPGProvider

    private val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)

            when (msg.what) {
                MSG_ON_GET_PAGES -> {
                    galleryAdapter = VPGAdapter(
                        galleryInfo!!,
                        provider,
                        msg.data.getInt(MSG_DATA_PAGES_KEY),
                        readDirection
                    )
                    galleryView.apply {
                        adapter = galleryAdapter
                        setCurrentItem((currentIndex + 1) / 2, false)
                        setProgressViews((currentIndex + 1) / 2 + 1)
                    }
                }
                MSG_ON_PAGE_DOWNLOAD -> {
                    galleryAdapter?.bindProgressFromCache(
                        msg.data.getInt(MSG_DATA_INDEX_KEY),
                        msg.data.getInt(MSG_DATA_PAGE_DOWNLOAD_PERCENT_KEY)
                    )
                }
                MSG_ON_GET_IMAGE_SUCCESS -> {
                    val index = msg.data.getInt(MSG_DATA_INDEX_KEY)
                    galleryAdapter?.bindBitmapFromCache(index)
                }
                MSG_ON_PAGE_FAILURE -> {
                    val index = msg.data.getInt(MSG_DATA_INDEX_KEY)
                    galleryAdapter?.bindErrorFromCache(index)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let { onRestore(it) } ?: onInit()

        if (galleryInfo == null) {
            finish()
        }

        initUi(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putString(GalleryActivity.KEY_ACTION, action)
            putString(GalleryActivity.KEY_FILENAME, filename)
            putParcelable(GalleryActivity.KEY_URI, uri)
            if (galleryInfo != null) {
                putParcelable(GalleryActivity.KEY_GALLERY_INFO, galleryInfo)
            }
            putInt(GalleryActivity.KEY_PAGE, page)
            putInt(GalleryActivity.KEY_CURRENT_INDEX, currentIndex)
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        SimpleHandler.getInstance().postDelayed({
            if (hasFocus && systemUiHelper != null) {
                if (showSystemUi) {
                    systemUiHelper!!.show()
                } else {
                    systemUiHelper!!.hide()
                }
            }
        }, 300)
    }

    override fun onDestroy() {
        super.onDestroy()
        galleryAdapter?.destroy()
        galleryAdapter = null
        galleryView.adapter = null
        provider.stop()
    }

    private fun onInit() {
        val intent = intent ?: return
        action = intent.action ?: action
        filename = intent.getStringExtra(GalleryActivity.KEY_FILENAME) ?: filename
        uri = intent.data ?: uri
        galleryInfo = intent.getParcelableExtra(GalleryActivity.KEY_GALLERY_INFO)
        page = intent.getIntExtra(GalleryActivity.KEY_PAGE, -1)
    }

    private fun onRestore(savedInstanceState: Bundle) {
        savedInstanceState.apply {
            action = getString(GalleryActivity.KEY_ACTION) ?: action
            filename = getString(GalleryActivity.KEY_FILENAME) ?: filename
            uri = getParcelable(GalleryActivity.KEY_URI) ?: uri
            galleryInfo = getParcelable(GalleryActivity.KEY_GALLERY_INFO)
            page = getInt(GalleryActivity.KEY_PAGE, -1)
            currentIndex = getInt(GalleryActivity.KEY_CURRENT_INDEX)
        }
    }

    /**
     * create main ui for the reader
     * based on [ViewPager2]
     */
    private fun initUi(savedInstanceState: Bundle?) {
        container = VPGContainer(this).apply {
            onMidTouchListener = {
                seekbar.hide()
                systemUiHelper?.let {
                    it.hide()
                    showSystemUi = false
                }
            }
            onDragListener = onMidTouchListener
            onBottomTouchListener = {
                seekbar.show()
                systemUiHelper?.let {
                    it.show()
                    showSystemUi = true
                }
            }
        }
        headerView = VPGHeaderView(this).also { it.visibilityBySettings() }
        seekbar = VPGSeekbar(this).apply {
            fitsSystemWindows = true
            layoutDirection = if (readDirection == GalleryView.LAYOUT_RIGHT_TO_LEFT) {
                View.LAYOUT_DIRECTION_RTL
            } else {
                View.LAYOUT_DIRECTION_LTR
            }
            onChangeListener = {
                val position = it - 1
                provider.putStartPage(position * 2)
                currentIndex = position * 2
                setProgressViews(position + 1)
                galleryView.setCurrentItem(position, false)
            }
        }

        provider = VPGProvider(handler, this, galleryInfo!!).also { it.start() }
        val startPage: Int = if (savedInstanceState == null) {
            if (page >= 0) page else provider.startPage
        } else {
            currentIndex
        }
        currentIndex = startPage
        provider.request(startPage)
        galleryView = ViewPager2(this).apply {
            if (provider.size() > 0) {
                galleryAdapter = VPGAdapter(galleryInfo!!, provider, provider.size(), readDirection)
                adapter = galleryAdapter
                setCurrentItem((currentIndex + 1) / 2, false)
                setProgressViews((currentIndex + 1) / 2 + 1)
            }
            layoutDirection = if (readDirection == GalleryView.LAYOUT_RIGHT_TO_LEFT) {
                View.LAYOUT_DIRECTION_RTL
            } else {
                View.LAYOUT_DIRECTION_LTR
            }
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    provider.putStartPage(position * 2)
                    currentIndex = position * 2
                    setProgressViews(position + 1)
                }
            })
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    if (parent.layoutDirection == View.LAYOUT_DIRECTION_RTL) {
                        outRect.left = LayoutUtils.dp2pix(EhApplication.getInstance(), 20F)
                    } else {
                        outRect.right = LayoutUtils.dp2pix(EhApplication.getInstance(), 20F)
                    }
                }
            })
        }

        container.addView(headerView, FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT))
        container.addView(
            galleryView, FrameLayout.LayoutParams(
                MATCH_PARENT, MATCH_PARENT
            ).apply {
                marginEnd = -LayoutUtils.dp2pix(EhApplication.getInstance(), 20F)
            }
        )
        container.addView(seekbar, FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            bottomMargin = LayoutUtils.dp2pix(this@VPGActivity, 16F)
            gravity = Gravity.BOTTOM.or(Gravity.CENTER_HORIZONTAL)
        })
        setContentView(container)
    }

    private fun setProgressViews(position: Int) {
        headerView.setProgress(
            position.toString() + "/" + galleryAdapter?.itemCount
        )
        seekbar.apply {
            setStart(position)
            setEnd(galleryAdapter?.itemCount ?: 999)
            seekbar.apply {
                val pages = (galleryAdapter?.itemCount ?: 999).toFloat()
                if (valueTo != pages) {
                    valueTo = pages
                }
                if (!isPressed) {
                    value = position.toFloat()
                }
            }
        }
    }
}