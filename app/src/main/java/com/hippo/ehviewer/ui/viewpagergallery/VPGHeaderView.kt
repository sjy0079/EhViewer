package com.hippo.ehviewer.ui.viewpagergallery

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.hippo.ehviewer.R
import com.hippo.ehviewer.Settings

class VPGHeaderView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {
    private val batteryView: View
    private val progressView: TextView
    private val clockView: View

    init {
        inflate(context, R.layout.vpg_main_header_layout, this)

        batteryView = findViewById(R.id.battery)
        progressView = findViewById(R.id.progress)
        clockView = findViewById(R.id.clock)
    }

    fun visibilityBySettings() {
        batteryView.visibility = if (Settings.getShowBattery()) VISIBLE else GONE
        progressView.visibility = if (Settings.getShowProgress()) VISIBLE else GONE
        clockView.visibility = if (Settings.getShowClock()) VISIBLE else GONE
    }

    fun setProgress(text: CharSequence) {
        progressView.text = text
    }
}
