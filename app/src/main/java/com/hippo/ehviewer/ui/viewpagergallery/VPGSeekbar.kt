package com.hippo.ehviewer.ui.viewpagergallery

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.LinearLayout.HORIZONTAL
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.slider.Slider
import com.hippo.yorozuya.LayoutUtils
import rikka.core.util.ResourceUtils.resolveColor

class VPGSeekbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    companion object {
        private const val AUTO_CLOSE_DELAY = 3000L
    }

    private var isAnimating = false
    private var lastPosition = -1
    private var autoCloseRunnable = Runnable {
        hide()
    }
    var onChangeListener: ((Int) -> Unit)? = null

    val seekbar = Slider(context).apply {
        valueFrom = 1F
        valueTo = 2F
        value = 1F
        setLabelFormatter {
            it.toInt().toString()
        }
        addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
                removeCallbacks(autoCloseRunnable)
            }

            override fun onStopTrackingTouch(slider: Slider) {
                value = value.toInt().toFloat()
                postDelayed(autoCloseRunnable, AUTO_CLOSE_DELAY)
            }
        })
        addOnChangeListener { _, value, fromUser ->
            if (!fromUser) {
                return@addOnChangeListener
            }
            startText.text = value.toInt().toString()
            if (lastPosition != value.toInt()) {
                lastPosition = value.toInt()
                onChangeListener?.invoke(lastPosition)
            }
        }
    }
    private val startText = generateProgressTextView().also { it.gravity = Gravity.END }
    private val endText = generateProgressTextView().also { it.gravity = Gravity.START }

    init {
        val realContainer = LinearLayout(context).apply {
            gravity = Gravity.CENTER
            orientation = HORIZONTAL
            background = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                cornerRadius = 9999F
                setColor(resolveColor(context.theme, android.R.attr.textColorSecondary))
            }
            addView(startText)
            addView(
                seekbar, LayoutParams(
                    LayoutUtils.dp2pix(context, 150F),
                    WRAP_CONTENT
                )
            )
            addView(endText)
        }
        addView(realContainer)
        visibility = View.GONE
    }

    fun setStart(position: Int) {
        startText.text = position.toString()
    }

    fun setEnd(maxCount: Int) {
        endText.text = maxCount.toString()
    }

    fun show() {
        if (isAnimating || visibility == View.VISIBLE) {
            return
        }
        removeCallbacks(autoCloseRunnable)
        visibility = View.VISIBLE
        alpha = 0F
        ObjectAnimator.ofFloat(0F, 1F).apply {
            duration = 300L
            addUpdateListener {
                alpha = it.animatedValue as Float
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    alpha = 1F
                    isAnimating = false
                }

                override fun onAnimationCancel(animation: Animator?) {
                    alpha = 1F
                    isAnimating = false
                }
            })
        }.start()
        isAnimating = true
        postDelayed(autoCloseRunnable, AUTO_CLOSE_DELAY)
    }

    fun hide() {
        if (seekbar.isPressed || isAnimating || visibility == View.GONE) {
            return
        }
        removeCallbacks(autoCloseRunnable)
        visibility = View.VISIBLE
        alpha = 1F
        ObjectAnimator.ofFloat(1F, 0F).apply {
            duration = 300L
            addUpdateListener {
                alpha = it.animatedValue as Float
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    alpha = 0F
                    visibility = View.GONE
                    isAnimating = false
                }

                override fun onAnimationCancel(animation: Animator?) {
                    alpha = 0F
                    visibility = View.GONE
                    isAnimating = false
                }
            })
        }.start()
        isAnimating = true
    }

    private fun generateProgressTextView() = AppCompatTextView(context).apply {
        width = LayoutUtils.dp2pix(context, 40F)
        textSize = 16F
        paint.isFakeBoldText = true
        setTextColor(resolveColor(context.theme, android.R.attr.colorBackground))
    }
}