package com.hippo.ehviewer.ui.viewpagergallery

import android.graphics.Bitmap
import android.os.Bundle

class VPGDetailActivity : VPGBasicActivity() {
    companion object {
        private const val RESTORE_KEY = "vpg_detail_bitmap_restore"
    }
    private var bitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            bitmap = it.getParcelable(RESTORE_KEY)
        } ?: run { bitmap = VPGImageView.transitionImage }

        setContentView(VPGPhotoView(this).apply {
            transitionName = VPGImageView.TRANS_NAME
            setImageBitmap(bitmap)
            post {
                init()
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(RESTORE_KEY, bitmap)
    }

    override fun onBackPressed() {
        finishAfterTransition()
    }
}